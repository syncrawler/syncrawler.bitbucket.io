
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
        <script type="text/javascript">
    
    //<![CDATA[
    
    /*
        */
    
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl+ '&gtm_auth=6ekrbC4Khu1PXBGChBz22w&gtm_preview=env-29&gtm_cookies_win=x';f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T3CG28');
    
    //]]>
    
</script>
		<meta charset="utf-8"/>
		<meta content="width=device-width,initial-scale=1.0" name="viewport"/>
		 <script id="syncrawler" src="http://localhost:3030/search" clientid="DKdS7tf2Vrpfl2RolynV"></script>

		<title>University of Dayton : University of Dayton, Ohio</title>
		
		   <meta content="d832cee50a480e990692655cf4eeb9e4" name="ud-page-id"/>
		
		<meta content="The University of Dayton is a top-tier national Catholic research university with a mission of service and leadership in community." name="description"/>
		<meta content="Fri, 22 Nov 2019 12:47:52 -0500" name="date"/>
		<meta content="INDEX,FOLLOW" name="ROBOTS"/>
<meta content="INDEX,FOLLOW" name="googlebot"/>
		<link href="https://udayton.edu/0/_2019/images/favicon.png" rel="shortcut icon" type="image/png"/>
        <link href="https://udayton.edu/0/_2019/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
		<link href="https://cloud.typography.com/6770292/6287212/css/fonts.css" rel="stylesheet" type="text/css"/>
		<link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,700" rel="stylesheet"/>
		<!-- Start CSS -->
		








	
<link href="/0/_2019/css/app.css" rel="stylesheet"/>



		<!-- End CSS -->
	</head>
	<body>
	    <!--GTM body starts-->
	    <div>
        	<noscript>
        		<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T3CG28&gtm_auth=6ekrbC4Khu1PXBGChBz22w&gtm_preview=env-29&gtm_cookies_win=x"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>        	</noscript>
        	<div class="google_translate">
        		<div id="google_translate_element"></div>
        	</div>
        </div>
	    <!--GTM body ends-->
		<div class="application__container" id="app">
            <the-spotlights class="application__animation-large"> </the-spotlights>
            

                
                                <div>


        
        
            
            
            
            
            
            
         
            
</div>


			<header class="the-header" id="the-header">
			    <a class="skip-main" href="#skipToContent" id="skiptoContentLink" onclick="ud.skipToContentReFocus()">Skip to main content</a>
				










<div class="menu__wrap menu__wrap--primary" is="MenuContainer">
    <div class="menu__header">
        <div class="menu__utility">
            <a class="menu__logo-link" href="/">
                                <svg class="header-logo" viewBox="0 0 1127.3 217" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <use xlink:href="/0/_2019/images/symbols/header-logo.svg#header-logo"/>
                </svg>
                <span class="access-hidden">Home</span>
            </a>
            <div class="menu__utility-large">
                <nav class="utility__nav">
                    <div class="search__controls" is="utility-search">
                        <slot name="search-icon">
                            <svg class="search-icon" viewBox="0 0 13.22 13.19" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/0/_2019/images/symbols/search-icon.svg#search-icon"/>
                            </svg>
                        </slot>
                    </div>
                    <ul class="utility__list">
                                                    
                                                                                                                                                    <li class="utility__list-item">
                                <a class="utility__list-link" href="learn.php">Learn</a>
                            </li>
                                                    
                                                                                                                                                    <li class="utility__list-item">
                                <a class="utility__list-link" href="lead.php">Lead</a>
                            </li>
                                                    
                                                                                                                                                    <li class="utility__list-item">
                                <a class="utility__list-link" href="serve.php">Serve</a>
                            </li>
                                                    
                                                                                                                                                    <li class="utility__list-item">
                                <a class="utility__list-link" href="apply/index.php">Apply</a>
                            </li>
                                                    
                                                                                                                                                    <li class="utility__list-item">
                                <a class="utility__list-link" href="/advancement/give/index.php">Give</a>
                            </li>
                                            </ul>
                </nav>
            </div>
        </div>
        <div class="menu__controls" is="MenuButton"></div>
    </div>
</div>
<h3> Keyword Search: <br></h3>
			<div id="div1"></div>	</header>
			










<script type="application/javascript">
  
  function isHidden(el) {
        var style = window.getComputedStyle(el);
        return (style.display === 'none')
  }

  function toggle_small_nav(id, obj) {
	    
		var this_item = document.getElementById(id);
		
		if (this_item !== null || this_item !== 'undefined'){
		    
		    var sizeForMenu = document.getElementsByClassName("one-menu__utility-small");
		    
		    if(typeof sizeForMenu[0] !== 'undefined'){
		        
		        if(!isHidden(sizeForMenu[0])){
		            var arrowDown = obj.getElementsByClassName("arrow__icon");		            
		            var panel = this.nextElementSibling;
                    if (this_item.style.maxHeight){
                        this_item.style.marginBottom = '0px';
                        this_item.style.maxHeight = null;
                        arrowDown[0].style.transform = 'rotate(0deg)';
                    } else {
                        this_item.style.marginBottom = '30px';
                        this_item.style.maxHeight = this_item.scrollHeight + "px";
                        arrowDown[0].style.transform = 'rotate(-90deg)';
                    } 
		        }
		    }
		}
  }
</script>

<nav class="menu menu-large">
    <tabs-container class="block">
        <div class="block__inner block__large">
            <div class="menu__utility-block one-menu__utility-small">
                <nav class="utility__nav">
                    <div class="search__controls" is="utility-search">
                        <slot name="search-icon">
                            <svg class="search-icon" viewBox="0 0 13.22 13.19" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/0/_2019/images/symbols/search-icon.svg#search-icon"/>
                            </svg>
                        </slot>
                    </div>
                    <ul class="utility__list utility__list-small">
                                                    
                                                                                                                                                    <li class="utility__list-item">
                                <a class="utility__list-link" href="learn.php">Learn</a>
                            </li>
                                                    
                                                                                                                                                    <li class="utility__list-item">
                                <a class="utility__list-link" href="lead.php">Lead</a>
                            </li>
                                                    
                                                                                                                                                    <li class="utility__list-item">
                                <a class="utility__list-link" href="serve.php">Serve</a>
                            </li>
                                                    
                                                                                                                                                    <li class="utility__list-item">
                                <a class="utility__list-link" href="apply/index.php">Apply</a>
                            </li>
                                                    
                                                                                                                                                    <li class="utility__list-item">
                                <a class="utility__list-link" href="/advancement/give/index.php">Give</a>
                            </li>
                                            </ul>
                </nav>
            </div>
            <tabs-container-tablist class="nav-block__tabs one-menu__body-small" orientation="vertical" tag="ul">
                                        <li class="nav-block__tabs__item">
                    <tabs-container-tablist-tab class="nav-block__button nav-block__button-active " controls="tabpanel__about" id="tab__about" onclick="event.preventDefault; toggle_small_nav('panel_about', this);">
                        <span class="accordion__label-btn--wrapper">
                            <span>About</span>
                            <div class="icon-accordion-wrap show-small-block">
                                <svg class="arrow__icon" viewBox="0 0 12.09 7.47" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <use xlink:href="/0/_2019/images/symbols/select-dropdown.svg#select-dropdown"/>
                                </svg>
                            </div>
                        </span>
                       
                    </tabs-container-tablist-tab>
                    <div class="accordion__panel show-small-block nav-accordion__panel" id="panel_about">
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="about/index.php">
                                University Overview
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="about/catholic-marianist.php">
                                Catholic, Marianist Education
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="about/points-of-pride.php">
                                Points of Pride
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="about/mission-and-identity.php">
                                Mission and Identity
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="about/history.php">
                                History
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="about/centers-and-institutes.php">
                                Centers and Institutes
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="about/partnerships.php">
                                Partnerships
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="life/city-of-dayton.php">
                                Location
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/directory/index.php">
                                Faculty and Staff Directory
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="social-directory.php">
                                Social Media Directory
                            </a>
                                            </div>
                </li>
                            <li class="nav-block__tabs__item">
                    <tabs-container-tablist-tab class="nav-block__button " controls="tabpanel__academics" id="tab__academics" onclick="event.preventDefault; toggle_small_nav('panel_academics', this);">
                        <span class="accordion__label-btn--wrapper">
                            <span>Academics</span>
                            <div class="icon-accordion-wrap show-small-block">
                                <svg class="arrow__icon" viewBox="0 0 12.09 7.47" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <use xlink:href="/0/_2019/images/symbols/select-dropdown.svg#select-dropdown"/>
                                </svg>
                            </div>
                        </span>
                       
                    </tabs-container-tablist-tab>
                    <div class="accordion__panel show-small-block nav-accordion__panel" id="panel_academics">
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="academics/index.php">
                                Academics Overview
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="academics/programs.php">
                                Program Listing
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="academics/90seconds.php">
                                90-Second Lectures
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/fss/important_dates/academic_calendar.php">
                                Academic Calendar
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/artssciences/index.php">
                                College of Arts and Sciences
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/business/index.php">
                                School of Business Administration
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/education/index.php">
                                School of Education and Health Sciences
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/engineering/index.php">
                                School of Engineering
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/law/index.php">
                                School of Law
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/continuing_education/index.php">
                                Professional and Continuing Education
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/international/iep/index.php">
                                Intensive English Program
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/libraries/index.php">
                                University Libraries
                            </a>
                                            </div>
                </li>
                            <li class="nav-block__tabs__item">
                    <tabs-container-tablist-tab class="nav-block__button " controls="tabpanel__admission" id="tab__admission" onclick="event.preventDefault; toggle_small_nav('panel_admission', this);">
                        <span class="accordion__label-btn--wrapper">
                            <span>Admission</span>
                            <div class="icon-accordion-wrap show-small-block">
                                <svg class="arrow__icon" viewBox="0 0 12.09 7.47" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <use xlink:href="/0/_2019/images/symbols/select-dropdown.svg#select-dropdown"/>
                                </svg>
                            </div>
                        </span>
                       
                    </tabs-container-tablist-tab>
                    <div class="accordion__panel show-small-block nav-accordion__panel" id="panel_admission">
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="apply/index.php">
                                Admission Overview
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="apply/undergraduate/index.php">
                                Undergraduate
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="apply/transfer/index.php">
                                Transfer
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="apply/international/index.php">
                                International
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="apply/graduate/index.php">
                                Graduate
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/law/admissions/index.php">
                                Law
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="apply/pce/index.php">
                                Professional and Continuing Education
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="apply/visit/index.php">
                                Campus Visit
                            </a>
                                            </div>
                </li>
                            <li class="nav-block__tabs__item">
                    <tabs-container-tablist-tab class="nav-block__button " controls="tabpanel__financial-aid-" id="tab__financial-aid-" onclick="event.preventDefault; toggle_small_nav('panel_financial-aid-', this);">
                        <span class="accordion__label-btn--wrapper">
                            <span>Financial Aid </span>
                            <div class="icon-accordion-wrap show-small-block">
                                <svg class="arrow__icon" viewBox="0 0 12.09 7.47" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <use xlink:href="/0/_2019/images/symbols/select-dropdown.svg#select-dropdown"/>
                                </svg>
                            </div>
                        </span>
                       
                    </tabs-container-tablist-tab>
                    <div class="accordion__panel show-small-block nav-accordion__panel" id="panel_financial-aid-">
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="affordability/index.php">
                                Affordability Overview
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="affordability/undergraduate/index.php">
                                Undergraduate
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="affordability/undergraduate/financial-aid/types-of-aid/transfer-student-aid.php">
                                Transfer
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="affordability/international.php">
                                International
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="affordability/graduate/index.php">
                                Graduate
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="affordability/law/index.php">
                                Law
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/finadmin/divisions/budget_planning/ir/consumer_info.php">
                                Consumer Information
                            </a>
                                            </div>
                </li>
                            <li class="nav-block__tabs__item">
                    <tabs-container-tablist-tab class="nav-block__button " controls="tabpanel__diversity" id="tab__diversity" onclick="event.preventDefault; toggle_small_nav('panel_diversity', this);">
                        <span class="accordion__label-btn--wrapper">
                            <span>Diversity</span>
                            <div class="icon-accordion-wrap show-small-block">
                                <svg class="arrow__icon" viewBox="0 0 12.09 7.47" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <use xlink:href="/0/_2019/images/symbols/select-dropdown.svg#select-dropdown"/>
                                </svg>
                            </div>
                        </span>
                       
                    </tabs-container-tablist-tab>
                    <div class="accordion__panel show-small-block nav-accordion__panel" id="panel_diversity">
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="about/diversity/index.php">
                                Diversity Overview
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/diversity/index.php">
                                Office of Diversity and Inclusion
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/finadmin/divisions/equity_compliance/index.php">
                                Equity Compliance Office
                            </a>
                                            </div>
                </li>
                            <li class="nav-block__tabs__item">
                    <tabs-container-tablist-tab class="nav-block__button " controls="tabpanel__research" id="tab__research" onclick="event.preventDefault; toggle_small_nav('panel_research', this);">
                        <span class="accordion__label-btn--wrapper">
                            <span>Research</span>
                            <div class="icon-accordion-wrap show-small-block">
                                <svg class="arrow__icon" viewBox="0 0 12.09 7.47" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <use xlink:href="/0/_2019/images/symbols/select-dropdown.svg#select-dropdown"/>
                                </svg>
                            </div>
                        </span>
                       
                    </tabs-container-tablist-tab>
                    <div class="accordion__panel show-small-block nav-accordion__panel" id="panel_research">
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="about/research/index.php">
                                Research Overview
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="momentum/index.php">
                                Momentum: Our Research
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/udri/index.php">
                                UD Research Institute
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/research/index.php">
                                Office for Research
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="/research/resourcesforresearchers/tpo/index.php">
                                Technology Transfer
                            </a>
                                            </div>
                </li>
                            <li class="nav-block__tabs__item">
                    <tabs-container-tablist-tab class="nav-block__button " controls="tabpanel__life-at-dayton" id="tab__life-at-dayton" onclick="event.preventDefault; toggle_small_nav('panel_life-at-dayton', this);">
                        <span class="accordion__label-btn--wrapper">
                            <span>Life at Dayton</span>
                            <div class="icon-accordion-wrap show-small-block">
                                <svg class="arrow__icon" viewBox="0 0 12.09 7.47" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <use xlink:href="/0/_2019/images/symbols/select-dropdown.svg#select-dropdown"/>
                                </svg>
                            </div>
                        </span>
                       
                    </tabs-container-tablist-tab>
                    <div class="accordion__panel show-small-block nav-accordion__panel" id="panel_life-at-dayton">
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="life/index.php">
                                Campus Overview
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="life/arts.php">
                                Arts
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="life/campus-recreation.php">
                                Campus Recreation
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="life/city-of-dayton.php">
                                City of Dayton
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="life/student-clubs.php">
                                Clubs and Organizations
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="life/housing-and-dining.php">
                                Housing and Dining
                            </a>
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="life/student-services.php">
                                Student Resources and Services
                            </a>
                                            </div>
                </li>
                            <li class="nav-block__tabs__item">
                    <tabs-container-tablist-tab class="nav-block__button " controls="tabpanel__athletics" id="tab__athletics" onclick="event.preventDefault; toggle_small_nav('panel_athletics', this);">
                        <span class="accordion__label-btn--wrapper">
                            <span>Athletics</span>
                            <div class="icon-accordion-wrap show-small-block">
                                <svg class="arrow__icon" viewBox="0 0 12.09 7.47" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <use xlink:href="/0/_2019/images/symbols/select-dropdown.svg#select-dropdown"/>
                                </svg>
                            </div>
                        </span>
                       
                    </tabs-container-tablist-tab>
                    <div class="accordion__panel show-small-block nav-accordion__panel" id="panel_athletics">
                                                    
                                                                                                                                                    <a class="block__copy panel__inner" href="athletics/index.php">
                                Athletics Overview
                            </a>
                                                    
                                                                                    <a class="block__copy panel__inner" href="https://daytonflyers.com/">
                                Dayton Flyers
                            </a>
                                            </div>
                </li>
                    </tabs-container-tablist>
            <div class="nav-block__main one-menu__body-small show-large-block">
                
                    <tabs-container-tabpanel class="nav-block__content" id="tabpanel__about" v-bind:transition-props="{name: 'fade'}" xmlns:v-bind="http://www.w3.org/1999/xhtml">
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="about/index.php">University Overview</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="about/catholic-marianist.php">Catholic, Marianist Education</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="about/points-of-pride.php">Points of Pride</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="about/mission-and-identity.php">Mission and Identity</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="about/history.php">History</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="about/centers-and-institutes.php">Centers and Institutes</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="about/partnerships.php">Partnerships</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="life/city-of-dayton.php">Location</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/directory/index.php">Faculty and Staff Directory</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="social-directory.php">Social Media Directory</a>
                                            </tabs-container-tabpanel>
                
                    <tabs-container-tabpanel class="nav-block__content" id="tabpanel__academics" v-bind:transition-props="{name: 'fade'}" xmlns:v-bind="http://www.w3.org/1999/xhtml">
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="academics/index.php">Academics Overview</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="academics/programs.php">Program Listing</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="academics/90seconds.php">90-Second Lectures</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/fss/important_dates/academic_calendar.php">Academic Calendar</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/artssciences/index.php">College of Arts and Sciences</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/business/index.php">School of Business Administration</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/education/index.php">School of Education and Health Sciences</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/engineering/index.php">School of Engineering</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/law/index.php">School of Law</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/continuing_education/index.php">Professional and Continuing Education</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/international/iep/index.php">Intensive English Program</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/libraries/index.php">University Libraries</a>
                                            </tabs-container-tabpanel>
                
                    <tabs-container-tabpanel class="nav-block__content" id="tabpanel__admission" v-bind:transition-props="{name: 'fade'}" xmlns:v-bind="http://www.w3.org/1999/xhtml">
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="apply/index.php">Admission Overview</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="apply/undergraduate/index.php">Undergraduate</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="apply/transfer/index.php">Transfer</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="apply/international/index.php">International</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="apply/graduate/index.php">Graduate</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/law/admissions/index.php">Law</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="apply/pce/index.php">Professional and Continuing Education</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="apply/visit/index.php">Campus Visit</a>
                                            </tabs-container-tabpanel>
                
                    <tabs-container-tabpanel class="nav-block__content" id="tabpanel__financial-aid-" v-bind:transition-props="{name: 'fade'}" xmlns:v-bind="http://www.w3.org/1999/xhtml">
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="affordability/index.php">Affordability Overview</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="affordability/undergraduate/index.php">Undergraduate</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="affordability/undergraduate/financial-aid/types-of-aid/transfer-student-aid.php">Transfer</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="affordability/international.php">International</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="affordability/graduate/index.php">Graduate</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="affordability/law/index.php">Law</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/finadmin/divisions/budget_planning/ir/consumer_info.php">Consumer Information</a>
                                            </tabs-container-tabpanel>
                
                    <tabs-container-tabpanel class="nav-block__content" id="tabpanel__diversity" v-bind:transition-props="{name: 'fade'}" xmlns:v-bind="http://www.w3.org/1999/xhtml">
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="about/diversity/index.php">Diversity Overview</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/diversity/index.php">Office of Diversity and Inclusion</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/finadmin/divisions/equity_compliance/index.php">Equity Compliance Office</a>
                                            </tabs-container-tabpanel>
                
                    <tabs-container-tabpanel class="nav-block__content" id="tabpanel__research" v-bind:transition-props="{name: 'fade'}" xmlns:v-bind="http://www.w3.org/1999/xhtml">
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="about/research/index.php">Research Overview</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="momentum/index.php">Momentum: Our Research</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/udri/index.php">UD Research Institute</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/research/index.php">Office for Research</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="/research/resourcesforresearchers/tpo/index.php">Technology Transfer</a>
                                            </tabs-container-tabpanel>
                
                    <tabs-container-tabpanel class="nav-block__content" id="tabpanel__life-at-dayton" v-bind:transition-props="{name: 'fade'}" xmlns:v-bind="http://www.w3.org/1999/xhtml">
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="life/index.php">Campus Overview</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="life/arts.php">Arts</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="life/campus-recreation.php">Campus Recreation</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="life/city-of-dayton.php">City of Dayton</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="life/student-clubs.php">Clubs and Organizations</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="life/housing-and-dining.php">Housing and Dining</a>
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="life/student-services.php">Student Resources and Services</a>
                                            </tabs-container-tabpanel>
                
                    <tabs-container-tabpanel class="nav-block__content" id="tabpanel__athletics" v-bind:transition-props="{name: 'fade'}" xmlns:v-bind="http://www.w3.org/1999/xhtml">
                                                    
                                                                                                                                                    <a class="nav-block__copy" href="athletics/index.php">Athletics Overview</a>
                                                    
                                                                                    <a class="nav-block__copy" href="https://daytonflyers.com/">Dayton Flyers</a>
                                            </tabs-container-tabpanel>
                            </div>
            <div class="nav-block__sidebar one-menu__block-sidebar one-menu__body-small">
                <div class="menu__sidebar-top">
                                        <h3 class="menu__sidebar-subheader">Next Steps</h3>
                    <ul class="list__link-list">
                                                                            <li class="list__link list__link-cta">
                                


        
                                            
    <a class="cta__featured cta__featured--light" href="apply/index.php">
    <span class="cta__featured-copy">Apply</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                            </li>
                                                    <li class="list__link list__link-cta">
                                


        
                                            
    <a class="cta__featured cta__featured--light" href="apply/visit/index.php">
    <span class="cta__featured-copy">Schedule a Visit</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                            </li>
                                                    <li class="list__link list__link-cta">
                                


        
                                            
    <a class="cta__featured cta__featured--light" href="apply/request-info.php">
    <span class="cta__featured-copy">Request Info</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                            </li>
                                                    <li class="list__link list__link-cta">
                                


        
                                            
    <a class="cta__featured cta__featured--light" href="/advancement/give/index.php">
    <span class="cta__featured-copy">Give</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                            </li>
                                            </ul>
                </div>
                <div class="menu__sidebar-bottom">
                    <div class="menu__sidebar-input-container">
                                                <h3 class="menu__sidebar-subheader">Info For:</h3>
                        <div class="select__input">
                            <select class="menu__sidebar--select" id="utilitylist" name="utilitylist" onchange="location = this.value;">
                                <option disabled="true" selected="true" value="Select Audience">Select Audience</option>
                                                                    
                                                                                                                                                                                            <option value="/advancement/index.php">Alumni and Friends</option>
                                                                    
                                                                                                                                                                                            <option value="for/current-students.php">Current Students</option>
                                                                    
                                                                                                                                                                                            <option value="apply/index.php">Future Students</option>
                                                                    
                                                                                                                                                                                            <option value="apply/undergraduate/guidance/index.php">High School Counselors</option>
                                                                    
                                                                                                                                                                                            <option value="for/international-students.php">International Students</option>
                                                                    
                                                                                                                                                                                            <option value="for/parents.php">Parents</option>
                                                                    
                                                                                                                                                                                            <option value="for/faculty-and-staff.php">Faculty and Staff</option>
                                                            </select>
                            <label class="utility-list__label" for="utilitylist">Select Audience</label>
                            <svg class="arrow__icon" viewBox="0 0 12.09 7.47" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/0/_2019/images/symbols/select-dropdown.svg#select-dropdown"/>
                            </svg>
                        </div>
                    </div>
                    <div class="menu__sidebar-links-container">
                                                <h3 class="menu__sidebar-subheader">Explore More</h3>
                        <ul class="list__link-list">
                                                    
                                                                                                                                                    <li class="list__link">
                                <a class="cta__basic" href="/news/index.php">
                                    <span class="cta__basic-copy">News</span>
                                </a>
                            </li>
                                                    
                                                                                                                                                    <li class="list__link">
                                <a class="cta__basic" href="/calendar/index.php">
                                    <span class="cta__basic-copy">Calendar</span>
                                </a>
                            </li>
                                                    
                                                                                                                                                    <li class="list__link">
                                <a class="cta__basic" href="/libraries/index.php">
                                    <span class="cta__basic-copy">Libraries</span>
                                </a>
                            </li>
                                                </ul>
                    </div>
                </div>
            </div>
            
        </div>
    </tabs-container>
</nav>

			
                                
        
                                    
    
			<div class="application" id="the-application">
				<div class="application__inner">
					<main class="the-main">
					    










        
        

        
    
            
                                                
                                                        
                                                        
                                                        
                                                        
                
        
                        
    <div class="home-hero__container">
        <video autoplay="true" class="hero__bg" loop="true" muted="true" playsinline="true" src="_resources/files/homepage-videos/chapel-sunset.mp4"></video>
                        
            <div class="home-hero__header-container">
            <h1 class="home-hero__header home-hero__headline-navy-gradient">Our Heart. Our Home.</h1>
        </div>    
    
            <div class="home-hero__cta-container home-hero__hero-links-black-gradient">
                            <ul class="home-hero__cta-column">
                    <li class="home-hero__cta-large home-hero__cta-top-space">
                <a class="cta__standard home-hero__cta-standard" href="learn.php">
                    <span class="home-hero__cta-header">Learn</span>
                    <span class="home-hero__cta-copy">Pursue new knowledge</span>
                </a>
            </li>
            <li class="home-hero__cta-small home-hero__cta-top-space">
                <a href="learn.php">
                    <span class="home-hero__cta-small-link">Learn</span>
                </a>
            </li>
            </ul>
                <ul class="home-hero__cta-column">
                    <li class="home-hero__cta-large home-hero__cta-top-space">
                <a class="cta__standard home-hero__cta-standard" href="lead.php">
                    <span class="home-hero__cta-header">Lead</span>
                    <span class="home-hero__cta-copy">Be a beacon for the world</span>
                </a>
            </li>
            <li class="home-hero__cta-small home-hero__cta-top-space">
                <a href="lead.php">
                    <span class="home-hero__cta-small-link">Lead</span>
                </a>
            </li>
            </ul>
                <ul class="home-hero__cta-column">
                    <li class="home-hero__cta-large home-hero__cta-top-space">
                <a class="cta__standard home-hero__cta-standard" href="serve.php">
                    <span class="home-hero__cta-header">Serve</span>
                    <span class="home-hero__cta-copy">Put others first, always</span>
                </a>
            </li>
            <li class="home-hero__cta-small home-hero__cta-top-space">
                <a href="serve.php">
                    <span class="home-hero__cta-small-link">Serve</span>
                </a>
            </li>
            </ul>
                <ul class="home-hero__cta-column">
                    <li class="home-hero__cta-large home-hero__cta-top-space">
                <a class="cta__standard home-hero__cta-standard" href="apply/index.php">
                    <span class="home-hero__cta-header">Apply</span>
                    <span class="home-hero__cta-copy">Take your first step</span>
                </a>
            </li>
            <li class="home-hero__cta-small home-hero__cta-top-space">
                <a href="apply/index.php">
                    <span class="home-hero__cta-small-link">Apply</span>
                </a>
            </li>
            </ul>
        </div>
        
</div>
        
					    
					    <a href="#" id="skipToContent"></a>
    					














                                                    
            
        
    
            <div class="first__module-small">
                
    
                        
                                        

 
                <div class="media-callout ">
            <div class="media-callout__container media-callout__container--large">
    
   <div class="media-callout__media media-callout__media-right ">

                                    
    
    
        
    <video-player video-id="F25vlAlAhAE" video-source="youtube"/>

            </div>
    
   
    <div class="media-callout__copy-wrapper media-callout__copy-wrapper-right ">
                    <div class="media-callout__kicker">Our Home is a Powerhouse</div>
                            <h2 class="media-callout__header">Knowledge is Power.</h2>
                                     <h3 class="media-callout__subheader">And Action is Powerful.</h3>
                            <p class="media-callout__copy">Whether they're building prosthetics for low-income patients in Bolivia, launching their own business or searching for ways to minimize the effects of brain cancer, UD students are encouraged to engage the world, developing a critical mind and a compassionate heart. Because when you learn by doing, the lessons last a lifetime.

</p>
                    </div>
</div>

    
    <div class="media-callout__container media-callout__container--small">
        <div class="media-callout__copy-wrapper">
                             <div class="media-callout__kicker">Our Home is a Powerhouse</div>
                                          <h2 class="media-callout__header">Knowledge is Power.</h2>
                     </div>
        <div class="media-callout__media media-callout__media-right ">
                                                
    
    
        
    <video-player video-id="F25vlAlAhAE" video-source="youtube"/>

                    </div>
        <div class="media-callout__copy-wrapper">
                                        <h3 class="media-callout__subheader">And Action is Powerful.</h3>
                                        <p class="media-callout__copy">Whether they're building prosthetics for low-income patients in Bolivia, launching their own business or searching for ways to minimize the effects of brain cancer, UD students are encouraged to engage the world, developing a critical mind and a compassionate heart. Because when you learn by doing, the lessons last a lifetime.

</p>
                                 </div>
    </div>

</div>
    
                                                                    
        
        
        
        
        
    
            </div>
        
                                                    
            
            
    
        
    
                        
                                                                             
        
    <div class="card__container">
                    <div class="card__gradient"></div>
                            <div class="card__kicker-container">
                <h2 class="card__kicker">Upcoming Events</h2>
            </div>
                <div class="card__cards-wrapper">
                
                            <div class="card__wrapper  card__three-cards ">
                    <div class="card__body card__body-no-image">
                
            <h2 class="card__pre-heading"/>            <div class="card__label">11.24.19</div>            <h1 class="card__subheader">Ebony Heritage Singers</h1>            <p class="card__copy"></p>            
                                                <a class="cta__standard" href="/calendar/2019/11/mus-11-24-ebony-heritage-singers.php">
    <span class="cta__copy">View Event</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>

                    </div>
    </div>
                
                            <div class="card__wrapper  card__three-cards ">
                    <div class="card__body card__body-no-image">
                
            <h2 class="card__pre-heading"/>            <div class="card__label">11.25.19</div>            <h1 class="card__subheader">Early Music Ensemble</h1>            <p class="card__copy"></p>            
                                                <a class="cta__standard" href="/calendar/2019/11/mus-11-25-early-music-ensemble.php">
    <span class="cta__copy">View Event</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>

                    </div>
    </div>
                
                            <div class="card__wrapper  card__three-cards ">
                    <div class="card__body card__body-no-image">
                
            <h2 class="card__pre-heading"/>            <div class="card__label">12.02.19</div>            <h1 class="card__subheader">At the Manger</h1>            <p class="card__copy"></p>            
                                                <a class="cta__standard" href="/calendar/2019/12/at-the-manger.php">
    <span class="cta__copy">View Event</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>

                    </div>
    </div>
                </div>
                <div class="card__cta-row">
            <div class="card__cta-column">
                               


        
                                            
    <a class="cta__standard" href="/calendar/index.php">
    <span class="cta__copy">View All Events</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


            </div>
        </div>
            
    
    </div>
                                
        
        
        
        
        
    
        
                                                    
            
        
    
        
    
                        
                            


<div class="news-listing__container">
            <div class="news-listing__kicker-container">
            <div class="news-listing__kicker">Latest News</div>
        </div>
        <div class="news-listing__content-container">
        
        
                                
               
                                                                                                                                                                                                          
        
        

   <div class="news-listing__content-wrapper">
            <div class="news-listing__image-wrapper">
                                                            <a class="news-listing__image-link" href="/news/articles/2019/11/fy2018_nsf_research_rankings.php">
                                            <img alt="" class="news-listing__image" src="/news/images/research/udri/c-130_600x400_05152019.jpg"/>
                                            </a>
                                                </div>
            <div class="news-listing__copy-wrapper">
                <div class="news-listing_subheader-wrapper">
                                                                <span class="news-listing__subheader-date">11.22.19</span>
                                    </div>
                                    <a class="news-listing__copy-header" href="/news/articles/2019/11/fy2018_nsf_research_rankings.php">UD moves into top spot in new category, remains in another in latest NSF rankings</a>
                                                    
                    <p class="news-listing__copy">
                        The University of Dayton moved into the top spot among all U.S. Catholic colleges and universities for research in physical science, technology, engineering and math (physical S/TEM), in the latest National Science Foundation rankings. In the rankings released Nov. 14 for research performed in fiscal year 2018, UD also retained its top spot for all materials research among all U.S. colleges and universities.
                    </p>
                                
                                    <a class="cta__standard" href="/news/articles/2019/11/fy2018_nsf_research_rankings.php">
    <span class="cta__copy">Read more</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>

            </div>
    </div>
        
        
                                
               
                                                                                                                                                                                                          
                

   <div class="news-listing__content-wrapper">
            <div class="news-listing__image-wrapper">
                                                            <a class="news-listing__image-link" href="/news/articles/2019/11/udsl_afrl_educational_partnership_patent_training.php">
                                            <img alt="" class="news-listing__image" src="/news/images/architecture/campus/keller_hall_600x400_03102015.jpg"/>
                                            </a>
                                                </div>
            <div class="news-listing__copy-wrapper">
                <div class="news-listing_subheader-wrapper">
                                                                <span class="news-listing__subheader-date">11.12.19</span>
                                    </div>
                                    <a class="news-listing__copy-header" href="/news/articles/2019/11/udsl_afrl_educational_partnership_patent_training.php">University of Dayton School of Law, Air Force Research Lab ink three-year educational partnership to provide patent training</a>
                                                    
                    <p class="news-listing__copy">
                        The University of Dayton School of Law and the U.S. Air Force Research Laboratory (AFRL) at Wright-Patterson Air Force Base have entered into a three-year educational partnership agreement for the School of Law to provide AFRL engineers and scientists legal training in the area of patents.
                    </p>
                                
                                    <a class="cta__standard" href="/news/articles/2019/11/udsl_afrl_educational_partnership_patent_training.php">
    <span class="cta__copy">Read more</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>

            </div>
    </div>
        
        
                                
               
                                                                                                                                                                                                                                
                

   <div class="news-listing__content-wrapper">
            <div class="news-listing__image-wrapper">
                                                            <a class="news-listing__image-link" href="/news/articles/2019/11/flyer_investments.php">
                                            <img alt="" class="news-listing__image" src="/news/images/academics/business/davis_center_600x400_081319.jpg"/>
                                            </a>
                                                </div>
            <div class="news-listing__copy-wrapper">
                <div class="news-listing_subheader-wrapper">
                                                                <span class="news-listing__subheader-date">11.04.19</span>
                                    </div>
                                    <a class="news-listing__copy-header" href="/news/articles/2019/11/flyer_investments.php">Student-led investment fund to manage additional $5 million for university</a>
                                                    
                    <p class="news-listing__copy">
                        The University of Dayton will move an additional $5 million of the institution’s endowment into a student-led investment fund over the next five semesters. The fund's current value is more than $35 million.
                    </p>
                                
                                    <a class="cta__standard" href="/news/articles/2019/11/flyer_investments.php">
    <span class="cta__copy">Read more</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>

            </div>
    </div>
            </div>
        <div class="news-listing__link-container">
        <div class="news-listing__link-wrapper">
                                                        


        
                                            
    <a class="cta__standard" href="/news/index.php">
    <span class="cta__copy">View All News</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                    </div>
    </div>
    </div>
                                                                                
        
        
        
        
        
    
        
                                                    
            
            
    
        
    
                        
                                                    

    
    <div class="discover-breaker__container">
        <div class="discover-breaker__background" style="background-image:url(_resources/img/home/test-tubes-udresearch-211.jpg)">
                            <div class="discover-breaker__gradient">
                                            <div class="gradient__background--glow">
        <div class="gradient__container--glow">
            <div class="glow1"></div>
            <div class="glow2"></div>
            <div class="glow3"></div>
        </div>
    </div>
                </div>
                    </div>
        <div class="discover-breaker__copy-wrapper">
            <div class="discover-breaker__copy-container">
                                    <div class="discover-breaker__kicker">Research</div>
                                <h2 class="discover-breaker__header">Our Volume of Research Speaks Volumes About Us</h2>
                <p class="discover-breaker__copy">Here, we don't just imagine a world of possibilities. We create it.</p>
                <div class="discover-breaker__ctas">
                                        


        
                                            
    <a class="cta__featured cta__featured--dark" href="about/research/index.php">
    <span class="cta__featured-copy">Explore Our Research</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                </div>
            </div>
        </div>
    </div>
                                                        
        
        
        
        
        
    
        
                                                    
            
        
    
        
    
                        
                                                                                                    
<tabs-container class="main__tab">
    <div class="main-tab__kicker-container">
        <h2 class="main-tab__kicker">Our Mission in Action</h2>
    </div>
    <div class="main-tab__wrapper">
        <tabs-container-tablist class="main-tab__tabs-container" orientation="horizontal" tag="ul">
                                        
                <li class="main-tab__tabs__item">
                                            <tabs-container-tablist-tab class="main-tab__button main-tab__button-active" controls="tabpanel__faith" id="tab__faith">
                                            
                        Faith
                    </tabs-container-tablist-tab>
                </li>
                            
                <li class="main-tab__tabs__item">
                                          <tabs-container-tablist-tab class="main-tab__button" controls="tabpanel__diversity-and-inclusion" id="tab__diversity-and-inclusion">
                                            
                        Diversity and Inclusion
                    </tabs-container-tablist-tab>
                </li>
                            
                <li class="main-tab__tabs__item">
                                          <tabs-container-tablist-tab class="main-tab__button" controls="tabpanel__arts" id="tab__arts">
                                            
                        Arts
                    </tabs-container-tablist-tab>
                </li>
                    </tabs-container-tablist>
        <div class="main-tab__inner">
                            <tabs-container-tabpanel class="main-tab__content" id="tabpanel__faith" v-bind:transition-props="{name: 'fade'}" xmlns:v-bind="http://www.w3.org/1999/xhtml">
                   
                   
                                        
                                            <img alt="Dove artwork in chapel" aria-hidden="true" class="main-tab__media" src="_resources/img/home/chapeldetails-465.jpg"/>
                                        <div class="main-tab__body-copy">
                                                <h1 class="card__subheader">Our light shines far and wide</h1>
                        <p class="card__copy">As a Catholic, Marianist university, we're deeply committed to the common good. Our faith is a beacon that guides us – and leads us to act. Regardless of your religious background, you'll find opportunities for personal and spiritual development.</p>
                                                                                                        


        
                                            
    <a class="cta__standard" href="about/catholic-marianist.php">
    <span class="cta__copy">Learn how our faith guides us</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                                            </div>
                    
                </tabs-container-tabpanel>
                            <tabs-container-tabpanel class="main-tab__content" id="tabpanel__diversity-and-inclusion" v-bind:transition-props="{name: 'fade'}" xmlns:v-bind="http://www.w3.org/1999/xhtml">
                   
                   
                                        
                                            <img alt="Diversity and inclusion" aria-hidden="true" class="main-tab__media" src="_resources/img/home/statue-dsc_1133.jpg"/>
                                        <div class="main-tab__body-copy">
                                                <h1 class="card__subheader">Where distinct paths converge</h1>
                        <p class="card__copy">Diversity is an intrinsic part of our DNA, and we offer programs and initiatives that advance inclusivity of all peoples and cultures. </p>
                                                                                                        


        
                                            
    <a class="cta__standard" href="about/diversity/index.php">
    <span class="cta__copy">Learn how our differences make us stronger</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                                            </div>
                    
                </tabs-container-tabpanel>
                            <tabs-container-tabpanel class="main-tab__content" id="tabpanel__arts" v-bind:transition-props="{name: 'fade'}" xmlns:v-bind="http://www.w3.org/1999/xhtml">
                   
                   
                                        
                                            <img alt="Arts" aria-hidden="true" class="main-tab__media" src="_resources/img/home/music-040417ud-0213.jpg"/>
                                        <div class="main-tab__body-copy">
                                                <h1 class="card__subheader">Elevating the Arts</h1>
                        <p class="card__copy">From visual art exhibitions to music, theatre and dance performances, we offer many opportunities and experiences, both for students and for the community.  </p>
                                                                                                        


        
                                            
    <a class="cta__standard" href="/arts/index.php">
    <span class="cta__copy">Learn about arts at UD</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                                            </div>
                    
                </tabs-container-tabpanel>
                    </div>
    </div>
</tabs-container>
        
        
        
        
        
        
    
        
                                                    
            
            
    
        
    
                        
                                                                                        


                
        
        
            
            <div class="related-links__container related-links__container--primary">
                        <div class="related-links__gradient">
                                    <div class="gradient__background--glow">
        <div class="gradient__container--glow">
            <div class="glow1"></div>
            <div class="glow2"></div>
            <div class="glow3"></div>
        </div>
    </div>
            </div>
                            <div class="related-links__kicker-container">
                                    <h2 class="related-links__kicker related-links__header--primary">Academics</h2>
                            </div>
                            <ul class="related-links__list">
                                                                    <li class="related-link__list-item">
                                    


        
                                            
    <a class="cta__featured cta__featured--dark" href="/artssciences/index.php">
    <span class="cta__featured-copy">College of Arts and Sciences</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                </li>
                                                <li class="related-link__list-item">
                                    


        
                                            
    <a class="cta__featured cta__featured--dark" href="/business/index.php">
    <span class="cta__featured-copy">School of Business Administration</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                </li>
                                                <li class="related-link__list-item">
                                    


        
                                            
    <a class="cta__featured cta__featured--dark" href="/education/index.php">
    <span class="cta__featured-copy">School of Education and Health Sciences</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                </li>
                                                <li class="related-link__list-item">
                                    


        
                                            
    <a class="cta__featured cta__featured--dark" href="/engineering/index.php">
    <span class="cta__featured-copy">School of Engineering</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                </li>
                                                <li class="related-link__list-item">
                                    


        
                                            
    <a class="cta__featured cta__featured--dark" href="/law/index.php">
    <span class="cta__featured-copy">School of Law</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                </li>
                                                <li class="related-link__list-item">
                                    


        
                                            
    <a class="cta__featured cta__featured--dark" href="/libraries/index.php">
    <span class="cta__featured-copy">University Libraries</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                </li>
                                                <li class="related-link__list-item">
                                    


        
                                            
    <a class="cta__featured cta__featured--dark" href="/continuing_education/index.php">
    <span class="cta__featured-copy">Continuing Education</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                </li>
                                                <li class="related-link__list-item">
                                    


        
                                            
    <a class="cta__featured cta__featured--dark" href="/centerforleadership/index.php">
    <span class="cta__featured-copy">Center for Leadership</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                </li>
                                                <li class="related-link__list-item">
                                    


        
                                            
    <a class="cta__featured cta__featured--dark" href="/international/iep/index.php">
    <span class="cta__featured-copy">Intensive English Program</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                </li>
                    </ul>
    </div>
    
            
        
        
        
        
        
    
        
                                                    
            
        
    
        
    
                        
                                                    

<div class="quote-block">
    <img alt="" class="quote-block__image" src="_resources/img/home/pillar-lester_091415_1689.jpg">
    <div class="quote-block__container">
        <div class="quote-block__gradient"></div>
        <blockquote class="quote__block__quote-container">
            <p class="quote__quote">The University of Dayton is a top-tier Catholic research university with offerings from the undergraduate to the doctoral levels. We are a diverse community committed, in the Marianist tradition, to educating the whole person and to linking learning and scholarship with leadership and service.</p>
            <footer class="quote__footer">
                <div class="quote__ingot"></div>
                <cite class="quote__cite">
                    <p class="quote__attrib"><strong>Our Mission</strong></p>
                                    </cite>
            </footer>
        </blockquote>
    </div>
    </img>
</div>

                                                                                    
        
        
        
        
        
    
        
                                                    
            
            
    
        
    
                        
                                                    

    
    <div class="discover-breaker__container">
        <div class="discover-breaker__background" style="background-image:url(_resources/img/home/map_uofd_phaseday2_0392.jpg)">
                            <div class="discover-breaker__gradient">
                                            <div class="gradient__background--glow">
        <div class="gradient__container--glow">
            <div class="glow1"></div>
            <div class="glow2"></div>
            <div class="glow3"></div>
        </div>
    </div>
                </div>
                    </div>
        <div class="discover-breaker__copy-wrapper">
            <div class="discover-breaker__copy-container">
                                    <div class="discover-breaker__kicker">Campaign</div>
                                <h2 class="discover-breaker__header">For Our Future</h2>
                <p class="discover-breaker__copy">Giving back is what Flyers do. We volunteer in community. We strengthen what we believe in through action and support. Now, Flyer Faithful are working toward a campaign for UD.</p>
                <div class="discover-breaker__ctas">
                                        


        
                                            
    <a class="cta__featured cta__featured--dark" href="impact.php">
    <span class="cta__featured-copy">How we're getting started</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                </div>
            </div>
        </div>
    </div>
                                                        
        
        
        
        
        
    
        
            

    

    <div class="card__sidebar-small-container">
                                                                                                        </div>

					</main>
				</div>
			</div>
			


















<footer class="the-footer">
<div class="the-footer__wrapper">
    <div class="the-footer__address">
        <a class="the-footer__home-cta" href="https://udayton.edu">
                       <svg class="footer-logo" viewBox="0 0 783.926 284.492" xmlns:xlink="http://www.w3.org/1999/xlink">
                <use xlink:href="/0/_2019/images/symbols/footer-logo.svg#footer-logo"/>
            </svg>
            <span class="access-hidden">Home</span>
        </a>
        <address>
            <div class="the-footer__address-meta" itemscope="" itemtype="Address">
               <span itemprop="location"> 300 College Park </span><br/> 
               <span itemprop="city"> Dayton, </span>
               <span itemprop="state"> Ohio </span>
               <span itemprop="zip"> 45469 </span><br/>
               <span itemprop="phone"> <a class="the-footer__address-cta" href="tel:937-229-1000">937-229-1000</a> </span>
            </div>
            <a class="the-footer__address-cta" href="mailto:info@udayton.edu">info@udayton.edu</a>
        </address>
        
        <ul class="list__link-list">
                                
                                    <li class="list__link">
            <a class="cta__basic" href="apply/visit/directions.php">
                <span class="cta__basic-copy">Directions</span>
            </a>
        </li>
    </ul>
    </div>
    <div class="the-footer__featured-ctas">
        <ul class="list__link-list">
                            <li class="list__link list__link-cta">
                    


        
                                            
    <a class="cta__featured cta__featured--dark" href="apply/index.php">
    <span class="cta__featured-copy">Apply</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                </li>
                            <li class="list__link list__link-cta">
                    


        
                                            
    <a class="cta__featured cta__featured--dark" href="apply/visit/index.php">
    <span class="cta__featured-copy">Visit</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                </li>
                            <li class="list__link list__link-cta">
                    


        
                                            
    <a class="cta__featured cta__featured--dark" href="apply/request-info.php">
    <span class="cta__featured-copy">Request Info</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                </li>
                            <li class="list__link list__link-cta">
                    


        
                                            
    <a class="cta__featured cta__featured--dark" href="/advancement/give/index.php">
    <span class="cta__featured-copy">Give</span>

    <span class="cta__arrow cta__arrow--internal">
        <svg class="arrow-icon" viewBox="0 0 13.75 12.16" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="/0/_2019/images/symbols/arrow-icon.svg#arrow-icon"/>
        </svg>
    </span>
    
</a>


                </li>
                    </ul>
        


<ul class="cta__social-links">
            


<li class="cta__social-links-container">
    
    <a class="cta__social-link cta__social-link_light" href="https://www.facebook.com/univofdayton/">
        <svg class="social-icon" viewBox="0 0 14 17" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <path d="M5.25,6.17H3.5V8.5H5.25v7H8.17v-7h2.12l.21-2.33H8.17v-1c0-.55.11-.77.65-.77H10.5V1.5H8.28c-2.1,0-3,.92-3,2.69Z"/>
                    </svg>
        <span class="access-hidden">Facebook</span>
    </a>
</li>


            


<li class="cta__social-links-container">
    
    <a class="cta__social-link cta__social-link_light" href="https://twitter.com/univofdayton">
        <svg class="social-icon" viewBox="0 0 14 17" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <path d="M13.5,4.28A5,5,0,0,1,12,4.72a2.76,2.76,0,0,0,1.17-1.54,5.31,5.31,0,0,1-1.69.68A2.6,2.6,0,0,0,9.5,3,2.77,2.77,0,0,0,6.9,6.4,7.49,7.49,0,0,1,1.41,3.49a2.86,2.86,0,0,0,.82,3.72A2.51,2.51,0,0,1,1,6.86,2.77,2.77,0,0,0,3.16,9.63a2.54,2.54,0,0,1-1.2,0,2.69,2.69,0,0,0,2.49,1.94A5.17,5.17,0,0,1,.5,12.77,7.32,7.32,0,0,0,4.59,14c4.95,0,7.75-4.38,7.58-8.3A5.75,5.75,0,0,0,13.5,4.28Z"/>
                    </svg>
        <span class="access-hidden">Twitter</span>
    </a>
</li>


            


<li class="cta__social-links-container">
    
    <a class="cta__social-link cta__social-link_light" href="https://www.instagram.com/universityofdayton/">
        <svg class="social-icon" viewBox="0 0 14 17" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <path d="M12,2,2,2a1.5,1.5,0,0,0-1.5,1.5l.06,10A1.5,1.5,0,0,0,2,15L12,15a1.5,1.5,0,0,0,1.49-1.51l-.06-10A1.5,1.5,0,0,0,12,2ZM7,6A2.5,2.5,0,1,1,4.5,8.51,2.5,2.5,0,0,1,7,6Zm4.91,6.85a.5.5,0,0,1-.5.5l-8.75.05a.5.5,0,0,1-.5-.5l0-5.46h1A4.36,4.36,0,0,0,3,8a5.27,5.27,0,0,0,0,.56,4,4,0,0,0,8,0A3.43,3.43,0,0,0,11,7.92a3,3,0,0,0-.11-.52h1Zm0-7.57a.5.5,0,0,1-.5.5H10.18a.51.51,0,0,1-.5-.5V4.11a.5.5,0,0,1,.5-.5h1.18a.5.5,0,0,1,.5.5Z" fill-rule="evenodd"/>
                    </svg>
        <span class="access-hidden">Instagram</span>
    </a>
</li>


            


<li class="cta__social-links-container">
    
    <a class="cta__social-link cta__social-link_light" href="https://www.youtube.com/UniversityOfDayton">
        <svg class="social-icon" viewBox="0 0 14 17" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <polygon fill-rule="evenodd" points="3.05 6.62 4.05 6.62 4.05 4.04 5.21 0.27 4.2 0.27 3.56 2.81 2.86 0.27 1.86 0.27 3.05 4.04 3.05 6.62"/>
    <path d="M5.17,5.45a1.14,1.14,0,0,0,1.3,1.26A1.21,1.21,0,0,0,7.75,5.45V3.15A1.24,1.24,0,0,0,6.47,1.89a1.21,1.21,0,0,0-1.3,1.26Zm.91-2.22c0-.26.12-.45.37-.45s.39.19.39.45V5.41c0,.26-.13.45-.37.45a.41.41,0,0,1-.39-.45Z" fill-rule="evenodd"/>
    <path d="M9.13,6.69a1.41,1.41,0,0,0,1-.59v.52H11V1.93h-.89V5.49c-.11.13-.35.35-.52.35s-.24-.12-.24-.31V1.93H8.51V5.85C8.51,6.31,8.65,6.69,9.13,6.69Z" fill-rule="evenodd"/>
    <path d="M12.07,12c0-.31-.07-.53-.37-.53s-.38.22-.38.53v.45h.75Z" fill-rule="evenodd"/>
    <path d="M8.4,11.47a.7.7,0,0,0-.17.14v2.78a.94.94,0,0,0,.2.16.37.37,0,0,0,.43-.05.5.5,0,0,0,.08-.31v-2.3a.58.58,0,0,0-.09-.35A.38.38,0,0,0,8.4,11.47Z" fill-rule="evenodd"/>
    <path d="M11.78,7.91C10,7.78,4,7.78,2.22,7.91.25,8,0,9.2,0,12.27s.24,4.24,2.22,4.37c1.82.13,7.74.13,9.56,0,2-.13,2.2-1.29,2.22-4.37S13.76,8,11.78,7.91ZM3.15,15.24H2.2V10.06h-1V9.19H4.14v.87h-1Zm3.4,0H5.7v-.49a1.81,1.81,0,0,1-.49.41c-.45.26-1.08.25-1.08-.65V10.77H5V14.2c0,.18,0,.3.22.3s.4-.21.5-.34V10.77h.85Zm3.26-.93h0c0,.56-.21,1-.78,1a1,1,0,0,1-.8-.4v.34H7.38V9.19h.85v1.94A1,1,0,0,1,9,10.72c.62,0,.83.51.83,1.12ZM12.94,14c0,.86-.38,1.38-1.27,1.38s-1.22-.58-1.22-1.38V12a1.24,1.24,0,0,1,1.28-1.32c.81,0,1.21.51,1.21,1.32v1.13H11.32V14c0,.34,0,.63.37.63s.38-.24.38-.63v-.31h.87Z" fill-rule="evenodd"/>
                    </svg>
        <span class="access-hidden">Youtube</span>
    </a>
</li>


            


<li class="cta__social-links-container">
    
    <a class="cta__social-link cta__social-link_light" href="https://www.linkedin.com/company/university-of-dayton">
        <svg class="social-icon" viewBox="0 0 14 17" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <path d="M3.2,3.68A1.35,1.35,0,1,1,1.85,2.34,1.34,1.34,0,0,1,3.2,3.68Zm-2.7,11H3.21V6.09H.5Zm7-8.57H4.84v8.57H7.53v-4.5c0-2.5,3.27-2.7,3.27,0v4.5h2.7V9.23c0-4.22-4.83-4.06-6-2Z" fill-rule="evenodd"/>
                    </svg>
        <span class="access-hidden">LinkedIn</span>
    </a>
</li>


    </ul>
    </div>
    <div class="the-footer__ctas">
        <ul class="list__link-list">
                                
                                    <li class="list__link">
            <a class="cta__basic" href="careers.php">
                <span class="cta__basic-copy">Careers</span>
            </a>
        </li>
                                
                                    <li class="list__link">
            <a class="cta__basic" href="about/contact.php">
                <span class="cta__basic-copy">Contact</span>
            </a>
        </li>
                                
                                    <li class="list__link">
            <a class="cta__basic" href="/bookstore/index.php">
                <span class="cta__basic-copy">Bookstore</span>
            </a>
        </li>
                                
                                    <li class="list__link">
            <a class="cta__basic" href="/advancement/alumni/index.php">
                <span class="cta__basic-copy">Alumni</span>
            </a>
        </li>
                                
                                    <li class="list__link">
            <a class="cta__basic" href="/libraries/index.php">
                <span class="cta__basic-copy">Libraries</span>
            </a>
        </li>
    </ul>
    </div>
    <div class="the-footer__image_container">
        <img alt="Welcome to the University of Dayton" aria-hidden="true" class="the-footer__image" src="_resources/img/home/chapel-footer.jpg"/>
    </div>
</div>
<div class="footer__anchor">
    <nav class="footer__anchor-nav">
        <ul class="footer__anchor-list">
                                                            <li class="footer__anchor-list-item">
                    <a class="footer__anchor-link" href="https://porches.udayton.edu"> PORCHES </a>
                </li>
                                                                                                    <li class="footer__anchor-list-item">
                    <a class="footer__anchor-link" href="/policies/index.php"> POLICIES </a>
                </li>
                                                                                                    <li class="footer__anchor-list-item">
                    <a class="footer__anchor-link" href="/arc/report_a_concern/index.php"> REPORT A CONCERN </a>
                </li>
                                                                                                    <li class="footer__anchor-list-item">
                    <a class="footer__anchor-link" href="terms/index.php"> PRIVACY &amp; TERMS </a>
                </li>
                                                                                                    <li class="footer__anchor-list-item">
                    <a class="footer__anchor-link" href="/finadmin/divisions/equity_compliance/about/index.php"> NONDISCRIMINATION </a>
                </li>
                                                                                                    <li class="footer__anchor-list-item">
                    <a class="footer__anchor-link" href="about/diversity/index.php"> DIVERSITY </a>
                </li>
                    </ul>
    </nav>
</div>
</footer>
		</div>
		
<script src="/0/_2019/js/manifest.js" type="text/javascript"></script>
<script src="/0/_2019/js/vendor.js" type="text/javascript"></script>
<script src="/0/_2019/js/app.js" type="text/javascript"></script>


<link href="/_udayton/scripts/rmodal/rmodal-no-bootstrap.css" rel="stylesheet" type="text/css"/>
<script src="/_udayton/scripts/rmodal/rmodal.min.js" type="text/javascript"></script>
<script src="/_udayton/scripts/rmodal/ud-modal.js" type="text/javascript"></script>

                                    
                 
                
                                    
                
                                    
                
                                    
                
                                    
                
                                    
                











	</body>
</html>