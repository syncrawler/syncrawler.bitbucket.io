University of Dayton

Department of Computer Science

CPS 491 - Capstone II, Fall 2019

Instructor: Dr. Phu Phung
 
# cps491f19-team1

## Team Members:

Morgan Weiss <weissm2@udayton.edu> (Team Leader)

Aaron Pirc <pirca1@udayton.edu> 

Josh Schmellenkamp <schmellenkampj1@udayton.edu>

Kevin Yeboah <yeboahk2@udayton.edu>

## Company Mentors
Ravish Kumar
Joydeep Mukherjee
Vish Dadireddy

Synchrony Financial 

950 Forrer Blvd, Kettering OH 45420

## Project Management Information 
Management board (private access): https://trello.com/b/NeXN7C1G/search-plug-in

Source code repository (private access): https://bitbucket.org/morganweiss33/cps491f19-team1/src/master/

Project Homepage (public): 

# Project Context and Scope:
Our project will be a plug-in for searching key words on a company's website.
For this a company must implement our JavaScript code in their own source code. 
After doing this Users of the company's site will be able to search key words, 
our search funtion will return the results of the search.

# System Analysis 

Sprint 1:
Currently we have a limited User Interface, and a semi-functioning back-end. The UI and back-end are not connected as of Sprint 1.
When the script is edited our back-end can take a [Key Word] from the user and search the contents of that website (given the URL is also 
input into the script).

Our first step in the future is to connect the back-end to the UI so you input the [Key Word] into the UI versus the script. 


# System Design

### Overview diagram
![Use Case Diagram] (Diagrams/sprint1UseCase2.jpg "Use Case Diagram")
![Use Case Diagram] (Diagrams/sprint1UseCase.jpg "Use Case Diagram")

## Database Information-
- We will be using Firebase by Google. Websites that insert our plug-in will be crawled via our crawler. That data will then be
stored in Firebase.

## User Interface Information-
- This will be a basic search bar, with the ability to be implemented on to any website. The results displayed will
be the first 10 results. The display will include a a short phrase from the website where the keyword is found. The keyword will be 
bolded. In addition, the url (including the path to the specific page) will be displayed to the user.

## User Requirements 

1. Must be very fast
2. Cannot use a traditional database
3. You must be able to plug it into any website


## Crawler Overview - 

1. Launch Heroku (therefore launching all servers)

2. Visit website with script already implemented, then enter [KEYWORD] to activate crawler (Front-end)

3. Crawler will make a request to website

4. Request will obtain data via html and pass it along to the web server 
    (Front-end connects/communicates with the back-end)

5. Server will "run" through the data and pull the info you want (keywords)

6. Clean the data to a format desired

7. Print the information via Front-end 
    (JSON, server log, .txt file, discuss with Synchrony)

# Implementation
- The website will input our plug-in script to their website, the company is then given
a company, through this we are able to tell what data is assocaited with which company
- By doing this we will now have one server that continously crawls their website
- When the server detects changes in the website they are then stored within Firebase



# Software Process Management
- We did the Agile Development Cycle, kept track of progress through Trello
- Met 3- 4 times to communicate about current progress and planned for future
- Spent a few days at Synchrony to generate idea


## Useful Websites-
- https://www.npmjs.com/package/flexsearch#installation (How to implement FlexSearch.js)
- https://fusejs.io/ (Fuzzy Search library for search efficiency)
- https://medium.com/dev-channel/how-to-add-full-text-search-to-your-website-4e9c80ce2bf4 (Ideas on past text search projects)
- https://code.tutsplus.com/tutorials/full-text-search-in-mongodb--cms-24835 (Text Indexing using MongDB)

# Sprint 1
Duration: 9/6 - 9/25 

## Completed Tasks-
- Established a weekly meeting time with with Synchrony (Friday at 11:15 AM)
- Added a basic User Interface
- Researched several topics related to implementing the project
- Completed re-worked the Server and User-Interface


## Contributions-
1. Morgan Weiss, 13 hours, contributed: Research MongoDB, created PowerPoint slides, worked to redo Server and UI 9/29

2. Josh Schmellenkamp, 13 hours, contributed: Researched fuzzy-search, worked to redo Server and UI 9/29

3. Aaron Pirc, 13 hours, contributed: Researched ElasticSearch + MongoDB Text Indexing, worked to redo Server and UI 9/29 

4. Kevin Yeboah, 13 hours, contibuted: worked to redo Server and UI 9/29 

### Sprint Retrospective:
We went into sprint1 very ambitious. In hind-sight we wish we would have scheduled another 
meeting after the first week of classes versus waiting until we began working indepently. 
We struggled a little with understanding the concept of the project. Once we understood the 
concept of the project it was easier to begin to implement. We still struggled with the idea 
of not using a database, but we are beginning to gain a better idea of what is required. 
As we continue to move forward we have scheduled meetings with Synchrony and we will be 
constantly reaching out with questions.


# Sprint 2
Duration: 9/26 - 10/28 

## Completed Tasks-
- Was able to get Apache Kafka running, can create topics
- Constructed server and client side communication for clientid
- Finalized design plans to begin implementing (includes a second server)

## Contributions-
1. Morgan Weiss, 16 hours, contributed: Created powerpoint slides, researched Apache Kafka, and worked to get testing environment worked on my machine

2. Josh Schmellenkamp, 16 hours, contributed: Researched and implemented kafka & researched pipeline techniques to connect kafka to node.js

3. Aaron Pirc, 16 hours, contributed: Researched kafka/zookeeper architecture.  Setup local Kafka server with consumers/producers to better understand the concept.

4. Kevin Yeboah, 16 hours, contibuted: worked on crawler job for the server. constructed server and client side communication for clientid

### Sprint Retrospective:
Looking at Sprint 2 we could have done somethings better. We really struggled to establish a solid option for storage. After meeting
with Synchrony we decided to proceed with Kafka as our storage option. This decision delayed our progess serverly. Designing 
the project and deciding what tools to use has taken a significant portion of our time. The biggest downfall of Sprint 2 is probably
timing. Designing and building the project at once is complicated for us. Going into Sprint 3 we are going to research and work with 
Synchrony more in order to get the project done in time. We are going to be focusing on storage and getting the user interface connected
to the storage of our choice. 

# Sprint 3
Duration: 10/29 - 12/4 

## Completed Tasks-
- Created Storage (Firebase)
- Search interface communicates with firebase clientid
- Search interface now deletes old searches on the website
- Finished Crawler
- Added scheduled timer to crawler
- Implemented Heroku

## Contributions-
1. Morgan Weiss, 15 hours, contributed: Created Mock Websites for testing, created search button, worked on formatting, Created powerpoint slides for final Presentation, updated ReadMe, made homepage for presentation

2. Josh Schmellenkamp, 15 hours, contributed: Implemented and deployed firebase server to Heroku & created front-end script website for assigning and copying ClientIDs

3. Aaron Pirc, 15 hours, contributed: Collected mock data (BestBuy) manually for firebase (before the crawler was implemented), Created Demo Video and fixed the button redirection, Updated readme for Sprint 3

4. Kevin Yeboah, 15 hours, contibuted: Worked on crawler and implemented the scheduled timer.  Fixed search function to now delete old searches on the website. 
Deployed servers to Heroku. Implemented a function to assign and copy ClientIDs for users to paste into their code.

### Sprint Retrospective:
Heading into Sprint 3 we knew we had to make major changes with our approach.  We decided to drop the kafka research and picked Firebase for our storage. Our first step was to
find mock html data and put it into our storage and connect the search function (pluggable code) to the clientID in firebase.  This was a success, for the search 
function could connect and display the [KEYWORD] searched for, as well as snippets around the word.  Our next task was to delete the mock data and actually populate
the database with live crawling data. At the end of Sprint 3 we wish we would have started with Firebase because it is efficient and was not very difficult to implement.
However, since we began using it in the last sprint there was a major time crunch to have our project at a point where we were proud of it. In the end, we are happy with our
final product, but with more time so much more could have been accomplished. 
