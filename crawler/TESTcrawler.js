var request = require('request')
var cheerio = require('cheerio')
var fs = require('fs')

const pageToVisit = "http://www.google.com"
var clientid = "001"

crawl(pageToVisit, clientid)

async function crawl(page, id) {
    let content
    let obj
    console.log(page)
    console.log("crawling...")
    
    request(page, async function(error, response, body) {
        console.log("called request of: " + page)
        if (error) {
            console.log("Error: " + error)
        }
        // check status code (200 is HTTP OK)
        console.log("Status code: " + response.statusCode)
        if (response.statusCode == 200) {
            //parse the document
            var $ = cheerio.load(body)
            content = $('html > body').text()
            console.log("*************CONTENT*************\n" + content)
            //store in json file
            obj = {
                url: page,
                clientid: id,
                content: content
            }
        }
    })

    console.log(content)
    setTimeout(crawl(page, id), 60000)
}
