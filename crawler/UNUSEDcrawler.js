const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path')

const request = require('request')
const cheerio = require('cheerio')

const app = express();

const port = process.env.port || 3333;

//middleware
app.use(bodyParser.json());
app.use(cors());

/*
//initialize server
app.listen(port, function () {
    console.log("Server started on port: " + port);
});
*/
/*
//request to initialize crawling of client
app.post('/crawl/:id/:page', async function(req, res) {
    console.log("got request to crawl client with clientid: " + req.params.id)
    //get client page from storage using client id
    const clientPage = req.params.page
    setInterval(crawler(clientPage, req.params.id), 15000)
    console.log("passed that")
    res.status(200)
    res.send()
})
*/
//crawler function
async function crawler(pageToVisit, id) {
  request(pageToVisit, function(error, response, body) {
    if (error) {
        //console.log("Error: " + error)
    }
    //check status code (200 is HTTP OK)
    console.log("Status code: " + response.statusCode + ", for page: " + pageToVisit)
    if (response.statusCode == 200) {
        //parse the document
        var $ = cheerio.load(body)
        var words = $('html > body').text()
        //store the words for the page with the page to visit or client id
        console.log(id)
    }
  })
  console.log("hello")
}
    